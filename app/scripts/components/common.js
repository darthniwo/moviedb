
const apiUrl = 'https://api.themoviedb.org/3';
const apiKey = '?api_key=09b9ece1e65a600aca53a9b246cedd9e';

window['movies'] = [];
window['maps'] = {};

const common = {
    urls: {
        api: 'https://api.themoviedb.org/3',
        genres: '/genre/movie/list',
        discover: '/discover/movie',
        search:'/search/movie'
    },
    toMap: (array, mapName, key) => {
        if (!window.maps[mapName]) {
            window.maps[mapName] = {};
        }

        for (let i = 0; i < array.length; i++) {
            window.maps[mapName][array[i][key]] = array[i];
        }
        return window.maps[mapName];
    },
    truncate: (str, chars) => {
        let text = str;
        let maxPos = chars;
        if (text.length > maxPos) {
            text = text.substr(0, maxPos) + '...';
            return text;
        } else {
            return str;
        }
    },
    getGenres: onSuccess => {

        fetch(common.urls.api + common.urls.genres + apiKey)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                if (typeof onSuccess === "function") {
                    onSuccess(data);
                }
            });
    },
    getList: (endpoint, onSuccess, extra = "", ) => {
        console.log(common.urls.api + endpoint + apiKey + extra)
        fetch(common.urls.api + endpoint + apiKey + extra)
            .then((response) => {
                return response.json();
            })
            .then((movie) => {
                if (typeof onSuccess === "function") {
                    onSuccess(movie);
                }
            });
    },

    // api + endpoin+ movieid + key + extras
    // api + endpoint + movieid + key + extras
    // api + /duscover/movie + '' + key + extras

    getDetail: (movieId, endpoint = "/movie/", onSuccess, extra = "", ) => {
        fetch(common.urls.api + endpoint + movieId + apiKey + extra)
            .then((response) => {
                return response.json();
            })
            .then((movie) => {
                if (typeof onSuccess === "function") {
                    onSuccess(movie);
                }
            });
    }
}

export default common;