import React, { Component } from 'react';
import Movie from './movie';

class Movies extends Component{
    render(){
        return (
            <div className="row movielist">
                {
                    this.props.movies_array.map((movie)=> {
                        return (
                            <Movie key={movie.id} movie={movie}/>
                        )
                    })
                }
            </div>
        )
    }
}

export default Movies;