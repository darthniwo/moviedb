import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Common from './common';

const Genres = ({genres_array}) => {
    const genres =  genres_array.map((id) =>{
            try{
                let genre = window.maps['genresMap'][id].name;
            }catch(e){
                console.log('error on:',id)
            }
            return(
                
                <span key={id}>{}</span>
            )
        })
    return (
       <span className="movie__genres">{genres}</span>
    )
}

class Movie extends Component {
    constructor(props){
        super(props);
        this.state = {
            movie: this.props.movie
        }
    }
    render() {
        return (
            <article className="movie col-sm-6">
                <div className="panel clearfix">
                    <div className="movie__poster">
                        <Link to={"/detail/" + this.state.movie.id}>
                            <img src={'http://image.tmdb.org/t/p/w500'+this.state.movie.poster_path} alt={this.state.movie.title} />
                        </Link>
                        
                    </div>
                    <div className="movie__info">
                        <header>
                            <h3 className="movie__title push--bottom">
                                <Link to={"/detail/" + this.state.movie.id}>
                                    {this.state.movie.original_title}
                                </Link>
                                <span className="pull-right">{this.state.movie.vote_average} <i className="fa fa-star"></i></span>
                            </h3>
                        </header>
                        <div className="movie__meta">
                            <span className="movie__year push-half--right">{this.state.movie.release_date.split('-')[0]}</span>
                            <span><Genres genres_array={this.state.movie.genre_ids} /></span>
                        </div>
                        <p className="movie__overview soft-half--top">{Common.truncate(this.state.movie.overview, 200)}</p>
                        <footer>
                            <Link to={"/detail/" + this.state.movie.id}>Mas informacion</Link>
                        </footer>
                    </div>
                </div>
            </article>
        );
    }
}

export default Movie;