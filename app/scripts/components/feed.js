import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Common from './common';
import Movies from './movies';
import Filter from './filters';


const List = ({ results_array }) => {

    if (typeof results_array != 'undefined') {
        const link_array = results_array.map((movie) => {
            return (
                <div key={'list-'+movie.id} className="result clearfix">
                    <Link to={'/detail/' + movie.id}>
                        <div className="result__img">
                            <img src={'http://image.tmdb.org/t/p/w92' + movie.poster_path} alt={movie.title} />
                        </div>
                        <div className="result__info">
                            <p>{movie.title}</p>
                            <span>{movie.release_date.split('-')[0]}</span>
                        </div>
                    </Link>
                </div>
            )
        })
        return (
            <div>{link_array}</div>
        )
    } else {
        return (
            <div>
                <p> Nada que ver señores!</p>
            </div>
        )
    }
}

class Feed extends Component {
    constructor(props) {
        super(props)
        this.state = {
            masterlist: [],
            search: [],
            genres: [],
            sort: [
                { val: '', label: 'Select sort', selected: true },
                { val: 'popularity.desc', label: 'Popularity Descending', selected: false },
                { val: 'popularity.asc', label: 'Popularity Ascending', selected: false },
                { val: 'vote_average.desc', label: 'Rating Descending', selected: false },
                { val: 'vote_average.asc', label: 'Rating Ascending', selected: false },
                { val: 'primary_release_date.desc', label: 'Release Date Descending', selected: false },
                { val: 'primary_release_date.asc', label: 'Release Date Ascending', selected: false },
                { val: 'title.asc', label: 'Title (A-Z)', selected: false },
                { val: 'title.desc', label: 'Title (Z-A)', selected: false }
            ],
            years: [
                { val: '', label: 'Select Year', selected: true },
                { val: '2017', label: '2017', selected: false },
                { val: '2016', label: '2016', selected: false },
                { val: '2015', label: '2015', selected: false },
                { val: '2014', label: '2014', selected: false },
                { val: '2013', label: '2013', selected: false },
                { val: '2012', label: '2012', selected: false },
                { val: '2011', label: '2011', selected: false },
                { val: '2010', label: '2010', selected: false },
                { val: '2009', label: '2009', selected: false },
                { val: '2008', label: '2008', selected: false },
                { val: '2007', label: '2007', selected: false },
                { val: '2006', label: '2006', selected: false },
                { val: '2005', label: '2005', selected: false },
                { val: '2004', label: '2004', selected: false },
                { val: '2003', label: '2003', selected: false },
                { val: '2002', label: '2002', selected: false },
                { val: '2001', label: '2001', selected: false },
                { val: '2000', label: '2000', selected: false },

            ],
            query: '',
            obj_filter: {
                year: '2017',
                genres: '',
                sort: 'popularity.desc'
            }
        }
    }
    componentWillMount() {

        let extra = '&sort_by=' + this.state.obj_filter.sort + '&primary_release_year=' + this.state.obj_filter.year + (this.state.obj_filter.genres !== '' ? '&with_genres=' + this.state.obj_filter.genres : '');
        Common.getList(Common.urls.discover, (movies) => {
            this.setState({ masterlist: movies.results })
            window.movies = movies.results;
            console.log(movies.results)
        }, extra)

        Common.getGenres((data) => {
            let genres_array = [];
            Common.toMap(data.genres, 'genresMap', 'id');
            genres_array.push({
                val: '',
                label: 'Select genre',
                selected: true
            });
            for (let genre of data.genres) {
                genres_array.push({
                    val: genre.id,
                    label: genre.name,
                    selected: false
                });
            }

            this.setState({ genres: genres_array })


        })
    }
    getFilteredList(val, category) {

        this.updateFilter(val, category, () => {
            let extra = '&sort_by=' + this.state.obj_filter.sort + '&primary_release_year=' + this.state.obj_filter.year + (this.state.obj_filter.genres !== '' ? '&with_genres=' + this.state.obj_filter.genres : '');
            Common.getList(Common.urls.discover, (movies => {
                this.setState({ masterlist: movies.results })
                window.movies = movies.results;
            }), extra)
        });

    }
    updateFilter(val, cat, cb) {

        switch (cat) {
            case 'primary_release_year':
                this.setState({
                    obj_filter: {
                        year: val,
                        sort: this.state.obj_filter.sort,
                        genres: this.state.obj_filter.genres
                    }
                }, cb)
                break;
            case 'sort_by':
                this.setState({
                    obj_filter: {
                        year: this.state.obj_filter.year,
                        sort: val,
                        genres: this.state.obj_filter.genres
                    }
                }, cb)
                break;
            case 'with_genres':
                this.setState({
                    obj_filter: {
                        year: this.state.obj_filter.year,
                        sort: this.state.obj_filter.sort,
                        genres: val
                    }
                }, cb)
                break;
        }
        console.log('filter:', this.state.obj_filter)
    }
    search(event) {
        if(event.target.value.length < 3){
           this.setState({ search: [] }) 
        }
        if (event.target.value.length >= 3){

            let query = '&query=' + event.target.value;
            console.log(Common.urls.search + query)
            Common.getList(Common.urls.search, (data) => {

                this.setState({ search: data.results })
            }, query)
        }
    }
    render() {

        return (
            <div>
                <div className="search">
                    <div className="wrapper">
                        <div className="search__input">
                            <i className="fa fa-search"></i>
                            <input type="text" placeholder="Search" onChange={this.search.bind(this)} />
                        </div>
                        <div className={"search__results "+ (this.state.search.length > 0 ? '':'hidden')}>
                            <List results_array={this.state.search} />
                        </div>
                    </div>
                </div>
                <div className="wrapper soft--top">
                    <h2 className="section-title push--bottom">Descubre Nuevas Peliculas</h2>
                    <div className="row filters">
                        <Filter key="year" category="primary_release_year" label="Año" options_array={this.state.years} filter={this.getFilteredList.bind(this)} />
                        <Filter key="sort" category="sort_by" label="Ordenar por" options_array={this.state.sort} filter={this.getFilteredList.bind(this)} />
                        <Filter key="genre" category="with_genres" label="Generos" options_array={this.state.genres} filter={this.getFilteredList.bind(this)} />
                    </div>
                    <Movies movies_array={this.state.masterlist} />
                </div>
            </div>

        )
    }
}

export default Feed;