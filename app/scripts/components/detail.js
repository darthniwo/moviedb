import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import YouTube from 'react-youtube';
import Common from './common';

const Cast = ({ cast_array }) => {
    if (typeof cast_array != 'undefined') {
        const members = cast_array.map((member) => {
            return (
                <li className="col-md-2" key={"cast-" + member.id}>
                    <div className="cast__img">
                        <img src={'https://image.tmdb.org/t/p/w138_and_h175_bestv2' + member.profile_path} alt={member.name} />
                    </div>
                    <div className="cast__info">
                        <span title={member.name}>{member.name}</span>
                        <span title={member.character}>{member.character}</span>
                    </div>
                </li>
            )
        })
        return (
            <div>{members}</div>
        )
    } else {
        return (
            <div>
                <p> Nada que ver señores!</p>
            </div>
        )
    }
}

const Pills = ({ array }) => {
    if (typeof array != 'undefined') {
        const pill_array = array.map((pill) => {
            return (
                <span key={"pill-" + pill.id}>{pill.name}</span>
            )
        })
        return (
            <div>{pill_array}</div>
        )
    } else {
        return (
            <div>
                <p> Nada que ver señores!</p>
            </div>
        )
    }
}

class Detail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            movie: {},
            credits: {},
            cast: [],
            keywords: {},
            trailer_id:''
        }
    }
    componentWillMount() {
        Common.getDetail(this.state.id, '/movie/', (movie) => {

            this.setState({ movie: movie });
            this.setState({ credits: movie.credits });
            this.setState({ keywords: movie.keywords });
            let array = [];
            for (let i = 0; i < 6; i++) {
                array.push(movie.credits.cast[i]);
            }
            this.setState({trailer_id: movie.videos.results[0].key});
            this.setState({ cast: array });
            console.log(movie)
        }, '&append_to_response=credits,keywords,videos')

    }
    render() {

        return (
            <div className="detail">
                <div className="detail__banner push--bottom">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-4">
                                <img src={'http://image.tmdb.org/t/p/w500' + this.state.movie.poster_path} alt={this.state.movie.title} />
                            </div>
                            <div className="col-sm-8">
                                <h2 className="detail__title">{this.state.movie.title} ({this.state.movie.release_date ? this.state.movie.release_date.split('-')[0] : ''})</h2>
                                <div className="detail__score">
                                    <span className="circle">{this.state.movie.vote_average}%</span>
                                    User Score
                                </div>
                                <div className="detail__info">
                                    <h3 className="push-half--bottom">Overview</h3>
                                    <p>{this.state.movie.overview}</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div className="wrapper">
                    <h3 className="push-half--bottom">Main Cast</h3>
                    <ul className="list-unstyled cast clearfix">
                        <Cast cast_array={this.state.cast} />
                    </ul>
                    <div className="data clear">
                        <h3>Data</h3>
                        <table className="table table-stripped hidden">
                            <thead>
                                <tr>
                                    <th>Original title</th>
                                    <th>Status</th>
                                    <th>Original Language</th>
                                    <th>Duration</th>
                                    <th>Budget</th>
                                    <th>Revenue</th>
                                    <th>Website</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{this.state.movie.original_title}</td>
                                    <td>{this.state.movie.status}</td>
                                    <td>{this.state.movie.original_language}</td>
                                    <td>{this.state.movie.runtime}</td>
                                    <td>${this.state.movie.budget}</td>
                                    <td>${this.state.movie.revenue}</td>
                                    <td>
                                        <a href={this.state.movie.homepage}>{this.state.movie.homepage ? Common.truncate(this.state.movie.homepage, 50) : ''}</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="col-md-6 hard flush">
                            <table className="table table-stripped">
                            <thead>
                                <tr>
                                    <th>Original title</th>
                                    <th>Status</th>
                                    <th>Original Language</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{this.state.movie.original_title}</td>
                                    <td>{this.state.movie.status}</td>
                                    <td>{this.state.movie.original_language}</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <div className="col-md-6 hard flush">
                            <table className="table table-stripped">
                            <thead>
                                <tr>
                                    <th>Duration</th>
                                    <th>Budget</th>
                                    <th>Revenue</th>
                                    <th>Website</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{this.state.movie.runtime}</td>
                                    <td>${this.state.movie.budget}</td>
                                    <td>${this.state.movie.revenue}</td>
                                    <td>
                                        <a href={this.state.movie.homepage}>{this.state.movie.homepage ? Common.truncate(this.state.movie.homepage, 50) : ''}</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <span className="subtitle">Genres</span>
                        <div className="data-pill push--bottom">
                            <Pills array={this.state.movie.genres} />
                        </div>
                        <span className="subtitle">Keywords</span>
                        <div className="data-pill push--bottom">
                            <Pills array={this.state.keywords.keywords} />
                        </div>
                    </div>
                    <div className="trailer">
                        <h3>Trailer</h3>
                        <YouTube
                            videoId={this.state.trailer_id}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default Detail;