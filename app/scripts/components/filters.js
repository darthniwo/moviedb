import React, { Component } from 'react';


class Filter extends Component {
    filterBy(event){
        
        this.props.filter(event.target.value, this.props.category)
    }
    render() {
        return (
            <div  className="form-group col-sm-3">
                <label>{this.props.label}</label>
                <select className="form-control" onChange={this.filterBy.bind(this)}>
                    {
                        this.props.options_array.map((item) => {
                            return (
                                <option key={item.label+'-'+item.id}  value={item.val} >{item.label}</option>
                            )
                        })
                    }
                </select>
            </div>
        )
    }
}

export default Filter;