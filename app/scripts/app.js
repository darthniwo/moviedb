import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import Feed from './components/feed';
import Detail from './components/detail'

ReactDOM.render(
  <BrowserRouter>
   <div>
      <Route exact path='/' component={Feed}/>
      <Route path='/detail/:id' component={Detail}/>
   </div>
  </BrowserRouter>
  ,
  document.getElementById('app')
);